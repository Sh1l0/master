function debounce(func, time) {
  let iTimeout = null
  return (...args) => {
    if(iTimeout) {
      window.clearTimeout(iTimeout)
    }
    iTimeout = window.setTimeout(() => {
      func(...args)
      iTimeout = null
    }, time)
  }
}



export default function ComboBox (name, placeholder, id, getItemsList) {

  let counter;

  function clearList (id) {
    const list = document.getElementById(`${id}_list`);
    if(!list) {
      return;
    }
    list.remove()
  }

  function renderLoad (id) {
    const list = document.createElement('div');
    list.className = 'list'
    list.id = id + '_list';

    const wrapper = document.createElement('div');
    wrapper.className = 'items-wrapper';
    wrapper.id = id + '_wrapper';

    const item = document.createElement('div');
    item.className = 'item';
    item.innerHTML = 'Загрузка';

    const allItems = document.querySelector(`#${id} > .combobox`);
    allItems.appendChild(list);
    list.appendChild(wrapper);
    wrapper.appendChild(item);

    return item;
  }

  function mouse (wrapper) {
    wrapper.addEventListener('mouseover', event => {
      if(document.querySelector('.selected')) {
        document.querySelector('.selected').classList.remove('selected');
      }
      const { target } = event;

      target.addEventListener('mousedown', event => {
        const value = event.target.innerHTML;
        target.parentNode.parentNode.previousSibling.value = value;
      });
      if(!target.classList.contains('item')) {
        return;
      }
      target.classList.add('selected');
      target.addEventListener('mouseout', event => {
        event.target.classList.remove('selected');
      });
    });
  }

  function keyboard () {
    const list = document.getElementById(id + '_list');
    if(!list) {
      return;
    }
    const input = list.previousSibling;
    const wrapper = document.getElementById(id + '_wrapper');
    const items = wrapper.children;
    if(!counter && counter !== 0) {
      counter = -1;
    }
    switch(event.keyCode) {
      case 40: {
        if(items[counter + 1]) {
          counter++;
        }
        if(document.querySelector('.selected')) {
          document.querySelector('.selected').classList.remove('selected');
        }
        if(counter > 6) {
          wrapper.scrollTop += 20;
        }
        if(!items[counter]) {
          break;
        }
        items[counter].classList.add('selected');
        break;
      }

      case 13: {
        input.value = document.querySelector('.selected').innerHTML;
        list.remove();
        break;
      }

      case 38: {
        if(counter < 4) {
          wrapper.scrollTop -= 20;
        }
        if(counter) {
          counter--;
        }
        if(document.querySelector('.selected')) {
          document.querySelector('.selected').classList.remove('selected');
        }
        items[counter].classList.add('selected');
        if(counter !== -1) {
          break;
        }
        counter = 0;
        break;
      }
    }
  }

  function updateItems (name, id, value) {
    clearList(id);

    const loading = renderLoad(id);
    const list = document.getElementById(id + '_list');
    const wrapper = document.getElementById(id + '_wrapper');
    const response = getItemsList(name, id, value)

    response.then(val => {
      console.log('список: ', val);
      loading.remove();
      list.innerHTML = "";
      if(val.length === 0) {
        list.appendChild(wrapper);
        wrapper.innerHTML = "Не найдено совпадений";
        return;
      }
      list.appendChild(wrapper);
      val.map((one) => {
        const div = document.createElement('div');
        div.className = 'item';
        div.innerHTML = one.name;
        wrapper.appendChild(div);
        if (list.previousSibling.value !== one.name) {
          return;
        }
        div.className += ' selected';
      });
    });
    mouse(wrapper);


    list.parentNode.addEventListener('keydown', keyboard)
  }

  function addListeners(input, name, id) {
    const updateItemsDebounce = debounce(updateItems, 1000);
    input.addEventListener('input', event => {
      updateItemsDebounce(name, id, input.value);
    });
    input.addEventListener('focus', () => {
      updateItems(name, id);
    });
    input.addEventListener('blur', () => {
      if(!input.nextSibling) {
        return;
      }
      input.nextSibling.remove();
    });
  }

  return {
    render: function () {
      const div = document.createElement('div');
      div.className = 'combobox';

      const input = document.createElement('input');
      input.name = name;
      input.placeholder = placeholder;
      input.type = 'text';

      const list = document.createElement('div');
      list.className = 'items-wrapper';
      list.id = id + '_wrapper';

      document.getElementById(id).appendChild(div)
      div.appendChild(input);

      addListeners(input, name, id, updateItems);
    }
  }
}



/* Реализуйте ComboBox и экспортируйте его при помощи export {ComboBox}*/
