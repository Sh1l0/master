import Combobox from './combobox.js'

const city = new Combobox('city', 'Введите город', 'row_city', getItemsList);
const ide = new Combobox('ide', 'Предпочитаемая среда', 'row_ide', getItemsList);

city.render();
ide.render();
/*

* В файле combobox.js реализуйте ComboBox
* а сюда импортируйте его, например так import {ComboBox} from './combobox.js'

/* Реализуйте функцю поиска городов */
function getItemsList(name, id, value) {
  let qParam = "";
  if(value) {
    qParam = `?name_like=${value}`;
  }

  return fetch(`/${name + qParam}`).then(res => {
    if(res.ok) {
      return res.json();
    }
    return new Promise.reject('Ошибка запроса');
  });
}
/* Добавьте ComboBox для строки "Город"
* например так:
* const city = new ComboBox('city', 'Введите или выберите из списка', getCityList)
* document.getElementById('row_city').appendChild(city.render())
* Ваша реализация может сильно отличаться
* */

/* Добавьте ComboBox для строки "Предпочитаемая IDE"*/
